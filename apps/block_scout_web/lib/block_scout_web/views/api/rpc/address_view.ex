defmodule BlockScoutWeb.API.RPC.AddressView do
  use BlockScoutWeb, :view

  alias BlockScoutWeb.API.RPC.RPCView

  def render("listaccounts.json", %{accounts: accounts}) do
    accounts = Enum.map(accounts, &prepare_account/1)
    RPCView.render("show.json", data: accounts)
  end

  def render("balance.json", %{addresses: [address]}) do
    RPCView.render("show.json", data: balance(address))
  end

  def render("balance.json", assigns) do
    render("balancemulti.json", assigns)
  end

  def render("balancemulti.json", %{addresses: addresses}) do
    data = Enum.map(addresses, &render_address/1)

    RPCView.render("show.json", data: data)
  end

  def render("txlist.json", %{transactions: transactions}) do
    data = Enum.map(transactions, &prepare_transaction/1)
    RPCView.render("show.json", data: data)
  end

  def render("txlistinternal.json", %{internal_transactions: internal_transactions}) do
    data = Enum.map(internal_transactions, &prepare_internal_transaction/1)
    RPCView.render("show.json", data: data)
  end

  def render("tokentx.json", %{token_transfers: token_transfers}) do
    data = Enum.map(token_transfers, &prepare_token_transfer/1)
    RPCView.render("show.json", data: data)
  end

  def render("tokenbalance.json", %{token_balance: token_balance}) do
    RPCView.render("show.json", data: to_string(token_balance))
  end

  def render("token_list.json", %{token_list: token_list}) do
    data = Enum.map(token_list, &prepare_token/1)
    RPCView.render("show.json", data: data)
  end

  def render("getminedblocks.json", %{blocks: blocks}) do
    data = Enum.map(blocks, &prepare_block/1)
    RPCView.render("show.json", data: data)
  end

  def render("error.json", assigns) do
    RPCView.render("error.json", assigns)
  end

  defp render_address(address) do
    %{
      "account" => "#{address.hash}",
      "balance" => balance(address),
      "stale" => address.stale? || false
    }
  end

  defp prepare_account(address) do
    %{
      "balance" => to_string(address.fetched_coin_balance && address.fetched_coin_balance.value),
      "address" => to_string(address.hash),
      "stale" => address.stale? || false
    }
  end

  defp prepare_transaction(transaction) do
    %{
      "blockHash" => "#{transaction.block_hash}",
      "blockNumber" => transaction.block_number,
      "confirmations" => transaction.confirmations,
      "contractAddress" => if(transaction.created_contract_address_hash != nil, do: "#{transaction.created_contract_address_hash}", else: nil),
      "cumulativeGasUsed" => Decimal.to_integer(transaction.cumulative_gas_used),
      "exchanger" => if(transaction.exchanger_address_hash != nil, do: "#{transaction.exchanger_address_hash}", else: nil),
      "exchangeRate" => if(transaction.exchange_rate != nil, do: "#{transaction.exchange_rate.value}", else: nil),
      "hash" => "#{transaction.hash}",
      "nonce" => transaction.nonce,
      "gas" => Decimal.to_integer(transaction.gas),
      "gasPrice" => "#{transaction.gas_price.value}",
      "gasUsed" => Decimal.to_integer(transaction.gas_used),
      "input" => "#{transaction.input}",
      "isError" => if(transaction.status == :ok, do: "0", else: "1"),
      "from" => "#{transaction.from_address_hash}",
      "setRateToken" => if(transaction.set_rate_token != nil, do: prepare_embedded_token(transaction.set_rate_token), else: nil),
      "timeStamp" => "#{DateTime.to_unix(transaction.block_timestamp)}",
      "to" => "#{transaction.to_address_hash}",
      "token" => if(transaction.token_address_hash != nil, do: "#{transaction.token_address_hash}", else: nil),
      "tokenTransferReceiver" => if(transaction.token_transfer_receiver_address_hash != nil, do: "#{transaction.token_transfer_receiver_address_hash}", else: nil),
      "tokenTransfers" => Enum.map(transaction.token_transfers, &prepare_embedded_token_transfer/1),
      "transactionIndex" => transaction.index,
      "txreceipt_status" => if(transaction.status == :ok, do: "1", else: "0"),
      "value" => "#{transaction.value.value}",
    }
  end

  # NAKA: Used for Transaction.token_transfers association
  defp prepare_embedded_token_transfer(token_transfer) do
    %{
      "from" => "#{token_transfer.from_address_hash}",
      "logIndex" => token_transfer.log_index,
      "payByToken" => token_transfer.pay_by_token,
      "to" => "#{token_transfer.to_address_hash}",
      "value" => get_token_value(token_transfer),
      "token" => prepare_embedded_token(token_transfer.token)
    }
  end

  # NAKA: Used for Transaction.set_rate_token associations
  defp prepare_embedded_token(token) do
    %{
      "contractAddress" => "#{token.contract_address_hash}",
      "name" => "#{token.name}",
      "symbol" => "#{token.symbol}",
      "decimals" => Decimal.to_integer(token.decimals),
      "totalSupply" => "#{token.total_supply}",
    }
  end

  defp prepare_internal_transaction(internal_transaction) do
    %{
      "blockNumber" => "#{internal_transaction.block_number}",
      "timeStamp" => "#{DateTime.to_unix(internal_transaction.block_timestamp)}",
      "from" => "#{internal_transaction.from_address_hash}",
      "to" => "#{internal_transaction.to_address_hash}",
      "value" => "#{internal_transaction.value.value}",
      "contractAddress" => "#{internal_transaction.created_contract_address_hash}",
      "transactionHash" => to_string(internal_transaction.transaction_hash),
      "index" => to_string(internal_transaction.index),
      "input" => "#{internal_transaction.input}",
      "type" => "#{internal_transaction.type}",
      "gas" => "#{internal_transaction.gas}",
      "gasUsed" => "#{internal_transaction.gas_used}",
      "isError" => if(internal_transaction.error, do: "1", else: "0"),
      "errCode" => "#{internal_transaction.error}"
    }
  end

  defp prepare_token_transfer(token_transfer) do
    %{
      "blockNumber" => to_string(token_transfer.block_number),
      "timeStamp" => to_string(DateTime.to_unix(token_transfer.block_timestamp)),
      "hash" => to_string(token_transfer.transaction_hash),
      "nonce" => to_string(token_transfer.transaction_nonce),
      "blockHash" => to_string(token_transfer.block_hash),
      "from" => to_string(token_transfer.from_address_hash),
      "contractAddress" => to_string(token_transfer.token_contract_address_hash),
      "to" => to_string(token_transfer.to_address_hash),
      "logIndex" => to_string(token_transfer.token_log_index),
      "value" => get_token_value(token_transfer),
      "tokenName" => token_transfer.token_name,
      "tokenSymbol" => token_transfer.token_symbol,
      "tokenDecimal" => to_string(token_transfer.token_decimals),
      "transactionIndex" => to_string(token_transfer.transaction_index),
      "gas" => to_string(token_transfer.transaction_gas),
      "gasPrice" => to_string(token_transfer.transaction_gas_price.value),
      "gasUsed" => to_string(token_transfer.transaction_gas_used),
      "cumulativeGasUsed" => to_string(token_transfer.transaction_cumulative_gas_used),
      "input" => to_string(token_transfer.transaction_input),
      "confirmations" => to_string(token_transfer.confirmations),
      "payByToken" => token_transfer.pay_by_token
    }
  end

  defp get_token_value(%{token_type: "ERC-721"} = token_transfer) do
    to_string(token_transfer.token_id)
  end

  defp get_token_value(token_transfer) do
    to_string(token_transfer.amount)
  end

  defp prepare_block(block) do
    %{
      "blockNumber" => to_string(block.number),
      "timeStamp" => to_string(block.timestamp),
      "blockReward" => to_string(block.reward.value)
    }
  end

  defp prepare_token(token) do
    %{
      "balance" => to_string(token.balance),
      "contractAddress" => to_string(token.contract_address_hash),
      "name" => token.name,
      "decimals" => to_string(token.decimals),
      "symbol" => token.symbol
    }
  end

  defp balance(address) do
    address.fetched_coin_balance && address.fetched_coin_balance.value && "#{address.fetched_coin_balance.value}"
  end
end
